# xalauc/php-composer

| PHP VERSION | TAGS        |
| ----------- | ----------- |
| 7.3         | latest, 7.3 |

This container allows for running composer against the same configuration as your PHP container running your
application so you can ensure that the dependencies are the same as your hosting environment.

## Environment Variables

A lot of the PHP.ini file is configurable using the Environment Variables for the docker machine, this allows for easy configuration of the PHP settings

> If you want to override any configuration that is not available as an environment variable you can always mount a volume to replace the files below and roll your own file without the need for the environment variables.

### PHP.ini

The file is located: `/etc/php7/php.ini`

#### Commonly Used

| VAR                     | php                 | Default Value                     |
| ----------------------- | ------------------- | --------------------------------- |
| PHP_DISPLAY_ERRORS      | display_errors      | 0                                 |
| PHP_ERROR_REPORTING     | error_reporting     | E_ALL & ~E_DEPRECATED & ~E_STRICT |
| PHP_MEMORY_LIMIT        | memory_limit        | 128M                              |
| PHP_MAX_INPUT_VARS      | max_input_vars      | 1000                              |
| PHP_DATE_TIMEZONE       | date.timezone       | Australia/Sydney                  |
| PHP_UPLOAD_MAX_FILESIZE | upload_max_filesize | 100M                              |
| PHP_POST_MAX_SIZE       | post_max_size       | 200M                              |
| PHP_HTML_ERRORS         | html_errors         | On                                |

#### The Rest

| VAR                                 | php                             | Default Value |
| ----------------------------------- | ------------------------------- | ------------- |
| PHP_DISPLAY_STARTUP_ERRORS          | display_startup_errors          | 0             |
| PHP_IGBINARY_COMPACT_STRINGS        | igbinary.compact_strings        | 1             |
| PHP_LOG_ERRORS                      | log_errors                      | 1             |
| PHP_LOG_ERRORS_MAX_LEN              | log_errors_max_len              | 10240         |
| PHP_MAX_EXECUTION_TIME              | max_execution_time              | 60            |
| PHP_MAX_FILE_UPLOADS                | max_file_uploads                | 20            |
| PHP_MAX_INPUT_TIME                  | max_input_time                  | 60            |
| PHP_OPCACHE_ENABLE                  | opcache.enable                  | 1             |
| PHP_OPCACHE_ENABLE_CLI              | opcache.enable_cli              | 0             |
| PHP_OPCACHE_MEMORY_CONSUMPTION      | opcache.memory_consumption      | 128           |
| PHP_OPCACHE_INTERNED_STRINGS_BUFFER | opcache.interned_strings_buffer | 32            |
| PHP_OPCACHE_MAX_ACCELERATED_FILES   | opcache.max_accelerated_files   | 10000         |
| PHP_OPCACHE_USE_CWD                 | opcache.use_cwd                 | 1             |
| PHP_OPCACHE_VALIDATE_TIMESTAMPS     | opcache.validate_timestamps     | 1             |
| PHP_OPCACHE_REVALIDATE_FREQ         | opcache.revalidate_freq         | 2             |
| PHP_OPCACHE_ENABLE_FILE_OVERRIDE    | opcache.enable_file_override    | 0             |
| PHP_OUTPUT_BUFFERING                | output_buffering                | 4096          |
| PHP_PRECISION                       | precision                       | -1            |
| PHP_REQUEST_ORDER                   | request_order                   | GP            |
| PHP_SERIALIZE_PRECISION             | serialize_precision             | -1            |
| PHP_SESSION_SERIALIZE_HANDLER       | session.serialize_handler       | php_binary    |
| PHP_SESSION_SAVE_HANDLER            | session.save_handler            | files         |
| PHP_SESSION_SAVE_PATH               | session.save_path               | /tmp          |
| PHP_SESSION_GC_PROBABILITY          | session.gc_probability          | 1             |
| PHP_SESSION_GC_DIVISOR              | session.gc_divisor              | 10000         |
| PHP_TRACK_ERRORS                    | track_errors                    | 0             |
| PHP_VARIABLES_ORDER                 | variables_order                 | GPCS          |
| PHP_ZEND_ASSERTIONS                 | zend.assertions                 | -1            |

### PHP.FPM

The file is located: `/etc/php7/php-fpm.d/www.conf`

| VAR                         | Option                  | Default Value |
| --------------------------- | ----------------------- | ------------- |
| PHP_PM                      | pm                      | ondemand      |
| PHP_PM_MAX_CHILDREN         | pm.max_children         | 100           |
| PHP_PM_START_SERVERS        | pm.start_servers        | 20            |
| PHP_PM_MIN_SPARE_SERVERS    | pm.min_spare_servers    | 20            |
| PHP_PM_MAX_SPARE_SERVERS    | pm.max_spare_servers    | 20            |
| PHP_PM_PROCESS_IDLE_TIMEOUT | pm.process_idle_timeout | 60s           |
| PHP_PM_MAX_REQUESTS         | pm.max_requests         | 500           |

### XDEBUG Only in the -dev versions

The file is located: `/etc/php7/conf.d/xdebug.ini`

| VAR                                  | php                                  | Default Value         |
| ------------------------------------ | ------------------------------------ | --------------------- |
| XDEBUG_REMOTE_ENABLE                 | xdebug.remote_enable                 | 0                     |
| XDEBUG_REMOTE_HANDLER                | xdebug_remote_handler                | dbgp                  |
| XDEBUG_REMOTE_CONNECT_BACK           | xdebug.remote_connect_back           | 0                     |
| XDEBUG_IDEKEY                        | xdebug.idekey                        | PHPSTORM              |
| XDEBUG_REMOTE_AUTOSTART              | xdebug.remote_autostart              | 0                     |
| XDEBUG_REMOTE_PORT                   | xdebug.remote_port                   | 9000                  |
| XDEBUG_REMOTE_HOST                   | xdebug.remote_host                   | localhost             |
| XDEBUG_AUTO_TRACE                    | xdebug.auto_trace                    | 0                     |
| XDEBUG_TRACE_ENABLE_TRIGGER          | xdebug.trace_enable_trigger          | 0                     |
| XDEBUG_TRACE_ENABLE_TRIGGER_VALUE    | xdebug.trace_enable_trigger_value    | ""                    |
| XDEBUG_TRACE_FORMAT                  | xdebug.trace_format                  | 0                     |
| XDEBUG_TRACE_OPTIONS                 | xdebug.trace_options                 | 0                     |
| XDEBUG_TRACE_OUTPUT_DIR              | xdebug.trace_output_dir              | /app/xdebug/trace/    |
| XDEBUG_TRACE_OUTPUT_NAME             | xdebug.trace_output_name             | trace.%c              |
| XDEBUG_PROFILER_AGGREGATE            | xdebug.profiler_aggregate            | 0                     |
| XDEBUG_PROFILER_APPEND               | xdebug.profiler_append               | 0                     |
| XDEBUG_PROFILER_ENABLE               | xdebug.profiler_enable               | 0                     |
| XDEBUG_PROFILER_ENABLE_TRIGGER       | xdebug.profiler_enable_trigger       | 0                     |
| XDEBUG_PROFILER_ENABLE_TRIGGER_VALUE | xdebug.profiler_enable_trigger_value | ""                    |
| XDEBUG_PROFILER_OUTPUT_DIR           | xdebug.profiler_output_dir           | /app/xdebug/profiler/ |
| XDEBUG_PROFILER_OUTPUT_NAME          | xdebug.profiler_output_name          | cachegrind.out.%p     |

## Defined Volumes

### /app

This volume contains the codebase please note that this is not necessarily the web root as that is defined as part of the web server configuration.

##  Exposed Ports

There are no exposed ports.

## Docker Command

To be able to run this using Docker itself you can perform the following:

```sh
docker run --rm --interactive --tty xalauc/php-composer:7.3 "COMMAND HERE"
```

To install you can run:
```sh
docker run --rm --interactive --tty xalauc/php-composer:7.3 "composer install"
```

## Docker Compose

Below is an example docker compose file using this image:

```yaml
version: '2'
services:
  composer:
    image: "xalauc/php-composer:7.3"
    restart: no
    volumes:
      - ./app:/app
    command: ["composer", "install"]
```

# License (MIT)

```
Copyright (c) 2018 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```