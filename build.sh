#!/bin/bash
echo "                                                                          "
echo "            ██╗  ██╗ █████╗ ██╗      █████╗ ██╗   ██╗ ██████╗             "
echo "            ╚██╗██╔╝██╔══██╗██║     ██╔══██╗██║   ██║██╔════╝             "
echo "             ╚███╔╝ ███████║██║     ███████║██║   ██║██║                  "
echo "             ██╔██╗ ██╔══██║██║     ██╔══██║██║   ██║██║                  "
echo "            ██╔╝ ██╗██║  ██║███████╗██║  ██║╚██████╔╝╚██████╗             "
echo "            ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝             "
echo "                                                                          "
echo "--------------------------------------------------------------------------"
echo "                                                                          "
echo "                          [ php-composer Builder ]                        "
echo "                                                                          "
echo "--------------------------------------------------------------------------"

if [ "$1" == "" ];
then
    echo ""
    echo "ERROR: You must enter a PHP version as the first parameter, try '7.3'"
    echo ""

    exit 1;
fi

if [ -d "./$1" ];
then
    echo "PHP version $1 found, building the image now."
    echo ""

    cd "$1"
    docker build -t "xalauc/php-composer:$1" .
else
    echo "That PHP version does not exist, please type another PHP version to attempt it."
fi
