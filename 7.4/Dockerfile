FROM php:7.4-cli-alpine
MAINTAINER Ben Blanks <ben@d15k.com>

# PHP INI config Options
ENV PHP_DATE_TIMEZONE=Australia/Sydney \
    PHP_DISPLAY_ERRORS=0 \
    PHP_HTML_ERRORS=On \
    PHP_ERROR_REPORTING="E_ALL & ~E_DEPRECATED & ~E_STRICT" \
    PHP_MEMORY_LIMIT=128M \
    PHP_MAX_INPUT_VARS=1000 \
    PHP_UPLOAD_MAX_FILESIZE=100M \
    PHP_POST_MAX_SIZE=200M \
    PHP_DISPLAY_STARTUP_ERRORS=0 \
    PHP_IGBINARY_COMPACT_STRINGS=1 \
    PHP_LOG_ERRORS=1 \
    PHP_LOG_ERRORS_MAX_LEN=10240 \
    PHP_MAX_EXECUTION_TIME=60 \
    PHP_MAX_FILE_UPLOADS=20 \
    PHP_MAX_INPUT_TIME=60 \
    PHP_OPCACHE_ENABLE=1 \
    PHP_OPCACHE_ENABLE_CLI=0 \
    PHP_OPCACHE_MEMORY_CONSUMPTION=128 \
    PHP_OPCACHE_INTERNED_STRINGS_BUFFER=32 \
    PHP_OPCACHE_MAX_ACCELERATED_FILES=10000 \
    PHP_OPCACHE_USE_CWD=1 \
    PHP_OPCACHE_VALIDATE_TIMESTAMPS=1 \
    PHP_OPCACHE_REVALIDATE_FREQ=2 \
    PHP_OPCACHE_ENABLE_FILE_OVERRIDE=0 \
    PHP_OUTPUT_BUFFERING=4096 \
    PHP_PM=ondemand \
    PHP_PM_MAX_CHILDREN=100 \
    PHP_PM_START_SERVERS=20 \
    PHP_PM_MIN_SPARE_SERVERS=20 \
    PHP_PM_MAX_SPARE_SERVERS=20 \
    PHP_PM_PROCESS_IDLE_TIMEOUT=60s \
    PHP_PM_MAX_REQUESTS=500 \
    PHP_PRECISION=-1 \
    PHP_REQUEST_ORDER=GP \
    PHP_SERIALIZE_PRECISION=-1 \
    PHP_SESSION_SERIALIZE_HANDLER=php_binary \
    PHP_SESSION_SAVE_HANDLER=files \
    PHP_SESSION_SAVE_PATH=/tmp \
    PHP_SESSION_GC_PROBABILITY=1 \
    PHP_SESSION_GC_DIVISOR=10000 \
    PHP_TRACK_ERRORS=0 \
    PHP_VARIABLES_ORDER=GPCS \
    PHP_ZEND_ASSERTIONS=-1 \
    PAGER="cat"

# Install Tools
RUN set -eux; \
    apk add --no-cache --virtual .composer-rundeps \
        coreutils \
        git \
        make \
        mercurial \
        openssh-client \
        patch \
        subversion \
        unzip \
        zip

# Ensure timezone is set
RUN apk --update add --no-cache tzdata \
    && cp /usr/share/zoneinfo/UTC /etc/localtime \
    && echo "UTC" > /etc/timezone \
    && apk del tzdata \
    && mkdir /docker-install.d/

# Install GD
RUN apk add --no-cache freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev \
    && docker-php-ext-configure gd \
    --with-freetype \
    --with-jpeg \
    && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j${NPROC} gd \
    && apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev

# Install Soap
RUN apk add --no-cache libxml2 libxml2-dev \
    && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j${NPROC} soap \
    && docker-php-ext-enable soap \
    && apk del --no-cache libxml2-dev

# Install mysqli
RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable mysqli pdo pdo_mysql

# Add intl support for PHP
RUN apk add --no-cache icu-libs zlib-dev icu-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-enable intl \
    && apk del --no-cache zlib-dev icu-dev

# TODO install pgsql, zip, mcrypt potentially

# Create User with ID 1000
RUN addgroup -g 1000 www && adduser -s sh -u 1000 -G www -D -h /home/www www

RUN EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)" \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")" \
    && if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]; then echo 'ERROR: Invalid installer signature'; fi \
    && php composer-setup.php --install-dir=/bin --filename=composer --quiet \
    && RESULT=$? \
    && rm composer-setup.php

COPY etc/php7/php.ini "${PHP_INI_DIR}/php.ini"
COPY bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod 0755 /usr/local/bin/docker-entrypoint.sh

VOLUME ["/app"]
WORKDIR /app

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["composer"]
